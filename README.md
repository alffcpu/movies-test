### Demo
  
http://show.thesuper.ru/  
  
### Production build  
  
1. npm i  
2. npm run build  

### Dev server  
  
1. npm i  
2. npm start  
  
### Run test  
  
npm test
  
### Task  

So what we’re gonna build today is an application for searching for just that,movies. The app must look good in desktop as well as mobile.  
  
Feature wise we’ll need:  
  
-- A search field and like a table for showing the result.  
-- It should also have functionality for setting movies as your favourites, maybe with a starsymbol in the table.  
-- A list where you can add movies you want to watch, like the “watch later” functionality onYouTube.  
-- Make Sure to test the application and write the tests in the Repo.  
-- The App Must Be working on desktop as well as mobile.  
-- Posters and trailers are always nice, this is however up to you.(Optional)  
-- Last but definitely NOT least, please include a Demo for us to see.  
  
To get the data for movies you can use any API you want, but if I were you, I'd use the APIprovided by TMDb: https://www.themoviedb.org/documentation/api
