import {getRequest} from './request';
import axios, {Canceler} from 'axios';
import trim from 'lodash/trim';
import {parseWatchId} from '../helpers/watchId';

type RCancel = Canceler | null;

export let CancelGetMovies: RCancel = null;

interface IGetMoviesRequest {
  query: string;
  page?: number;
}

const CancelToken = axios.CancelToken;
export const getMovies = async ({query, page = 1}: IGetMoviesRequest) =>
  getRequest('search/multi', {
    query: trim(query),
    page,
    cancelToken: new CancelToken((c) => CancelGetMovies = c)
  });

export const getMedia = async (itemId: string) => {
  const {id, mediaType} = parseWatchId(itemId);
  return getRequest(`${mediaType}/${id}`, {append_to_response: 'videos'});
};

export const getMediaVideos = async (type: string, id: number) =>
  getRequest(`${type}/${id}/videos`);
