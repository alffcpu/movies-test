import axios, {AxiosPromise, AxiosRequestConfig} from 'axios';
import trim from 'lodash/trim'
import {IKeyValue} from 'types/interfaces';

const {REACT_APP_TMDB_API_URL: url, REACT_APP_TMDB_TOKEN: token, REACT_APP_TMDB_API_LANG: lang} = process.env;

const serviceParams: IKeyValue = {
  cancelToken: true,
};

const apiUrl = (method: string, params: IKeyValue = {}): string => {
  const values = Object.entries(params).filter((item) => !serviceParams[item[0]]);
  const paramsString = values.map(([key, value]) => `${key}=${value}`).join('&');
  return `${url}${trim(method, '/')}?api_key=${token}&language=${lang}${paramsString ? `&${paramsString}` : ''}`;
};

export const getRequest = <T = any>(path: string, params?: T, config?: AxiosRequestConfig): AxiosPromise =>
  axios.get(apiUrl(path, params), config);

export const postRequest = <T = any>(path: string, data: T, config?: AxiosRequestConfig): AxiosPromise =>
  axios.post(apiUrl(path), data, config);

