import searchResults from 'stubs/search_stargate';
import {fetchGenres} from './genre';
import axios from 'axios';
import allGenres, {movie_genres, tv_genres} from 'stubs/genres';
import sleep from 'helpers/sleep';
import {getMovies, getMedia, getMediaVideos} from './search';

describe('Backend endpoints', () => {

  it('fetch genres', async () => {

    jest.spyOn(axios, 'get').mockImplementation(async (url: string) => {
      await sleep(50);
      return {
        data: url.indexOf('/genre/movie/') !== -1 ?
          movie_genres :
          tv_genres
      };
    });
    const genres = await fetchGenres();
    expect(genres).toEqual(allGenres);
  });

});
