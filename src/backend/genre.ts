import {getRequest} from './request';
import {AxiosResponse} from 'axios';
import {IGenresData} from 'types/interfaces';

export const getMovieGenres = () =>
  getRequest('genre/movie/list');

export const getTvGenres = () =>
  getRequest('genre/tv/list');

export const fetchGenres = async () => {
  const {data: movieGenres}: AxiosResponse<IGenresData> = await getMovieGenres();
  const {data: tvGenres}: AxiosResponse<IGenresData> = await getTvGenres();
  return [...movieGenres.genres, ...tvGenres.genres].reduce((result, genre) => ({
    ...result,
    [genre.id]: genre
  }), {});
};
