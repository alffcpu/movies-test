import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach} from 'test-utils';
import App from './App';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Layout rendering', () => {
  it('with skeleton', () => {
    act(() => {
      render(<App/>, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
