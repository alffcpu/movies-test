import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import StateProvider from 'state';
import ScrollToTop from 'components/ScrollToTop';

import 'antd/dist/antd.css';
import './assets/styles.scss';

ReactDOM.render(
  <BrowserRouter>
    <ScrollToTop />
    <StateProvider>
      <App />
    </StateProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

