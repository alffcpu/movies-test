import {SetStateValueCallback} from '../state/store';
import {DispatchAny} from '../App';
import chunk from 'lodash/chunk';
import {IMedia, IMediaDetailsResponse} from '../types/interfaces';
import {parseWatchId} from 'helpers/watchId';
import {getMedia} from 'backend/search';
import {getErrorMessage} from 'helpers/displayResponseError';
import {useCallback, useState} from 'react';
import useStore from './useStore';
import {ListName, watchPerPage} from 'components/ShowList/ShowList';

type FetckKeys = [string, string, string, string];

const defaultFetchKeys: FetckKeys = ['detailedList', 'detailedListName', 'detailedListPage', 'detailedListTotal'];
const fetchDetailedList = async (list: string[], listName: ListName, page = 1, setStateValue: SetStateValueCallback, setPending: DispatchAny<boolean>, keys: FetckKeys = defaultFetchKeys) => {
  const arrPage = page ? page - 1 : 0;
  const pagedData = chunk(list, watchPerPage);
  const pagedList = pagedData[arrPage];

  setPending(true);
  const result: IMedia[] = [];
  const fetcher = async (itemId: string, index: number) => {
    const {id, mediaType} = parseWatchId(itemId);
    const media_type = mediaType === 'tv' ? mediaType : 'movie';
    try {
      const {data}: IMediaDetailsResponse = await getMedia(itemId);
      const genre_ids = data.genres.map((g) => g.id);
      result[index] = {...data, genre_ids, media_type};
    } catch (error) {
      result[index] = {
        error: getErrorMessage(error),
        id: parseInt(id, 10) || 0,
        media_type
      } as IMedia;
    }
  };
  if (pagedList) {
    const queue = pagedList.map((itemId, index) => fetcher(itemId, index));
    await Promise.all(queue);
  }
  const [listKey, ListNameKey, pageKey, totalKey] = keys;
  setStateValue({
    [listKey]: result,
    [ListNameKey]: listName,
    [pageKey]: page,
    [totalKey]: list.length
  });
  setPending(false);

};

const useFetchDetails = (listName: ListName) =>  {
  const [pending, setPending] = useState(false);
  const {setStateValue, detailedListTotal, detailedListPage, watchList, favoritesList} = useStore();
  const showList = listName === 'watchList' ? watchList : favoritesList;
  const fetchList = useCallback((page: number) =>
      fetchDetailedList(showList, listName, page, setStateValue, setPending),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [showList]
  );
  return {
    pending,
    fetchList,
    detailedListTotal,
    detailedListPage,
    setStateValue,
    showList
  };
};

export default useFetchDetails;
