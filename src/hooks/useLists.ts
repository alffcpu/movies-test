import {IMedia} from '../types/interfaces';
import {getWatchId} from 'helpers/watchId';
import {useMemo} from 'react';

const useLists = (movie: IMedia, favoritesList: string[], watchList: string[]) => {
  const watchId = getWatchId(movie);
  const isFav = useMemo(() => favoritesList.indexOf(watchId) !== -1, [favoritesList, watchId]);
  const isWatch = useMemo(() => watchList.indexOf(watchId) !== -1, [watchList, watchId]);
  return {watchId, isFav, isWatch};
};

export default useLists;
