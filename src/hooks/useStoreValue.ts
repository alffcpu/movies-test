import {useContext} from 'react';
import {AppStateContext} from 'state';
import {IState} from 'state/store';
import { SetStateValueCallback } from 'state/store';
import { ValueOf } from 'types/interfaces';

const useStoreValue = (key?: keyof IState): [ValueOf<IState> | undefined, SetStateValueCallback]=> {
  const ctx = useContext(AppStateContext);
  const returnValue = key ? ctx[key] : undefined;
  return [returnValue, ctx.setStateValue];
};

export default useStoreValue;
