import {useEffect, useState} from 'react';
import useStore from './useStore';

const useDetailedPageReset = () => {
  const {setStateValue} = useStore();
  const [ready, setReady] = useState(false);
  useEffect(() => {
    setStateValue({detailedListPage: 1, detailedListTotal: null});
    setReady(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return ready;
};

export default useDetailedPageReset;
