export interface IKeyValue<T = any> {
  [key: string]: T;
}

export interface IGenre {
  id: number,
  name: string;
}

export interface IGenres {
  [key: number]: IGenre,
}

export type ValueOf<T> = T[keyof T];

export interface IGenresData {
  genres: IGenre[];
}

export interface IPagedResultData {
  page: number;
  total_results: number;
  total_pages: number;
}

type MediaType = 'movie' | 'tv' | 'person';

export interface IBaseMedia {
  adult?: boolean;
  backdrop_path: string;
  id: number;
  original_language: string;
  overview: string;
  popularity: number;​​
  poster_path: string;
  release_date?: string;
  first_air_date?: string;
  title?: string;
  vote_average: number;
  vote_count: number;
  media_type: MediaType;
  known_for?: IMedia[];
  original_title?: string;
  original_name?: string;
  name?: string;
  error?: string;
  videos?: {
    results: IVideo[];
  };
  origin_country?: string[];
}

export interface IMedia extends IBaseMedia {
  genre_ids: number[];
}

export interface IMediaDetailed extends IBaseMedia {
  genres: IGenre[];
  genre_ids: number[];
}

export interface ISearchMoviesData extends IPagedResultData {
  results: IMedia[];
}

type VideoSite = 'YouTube';

export interface IVideo {
  id: string;
  key: string;
  name: string;
  site: VideoSite
  size: number;
  type: string;
}

export interface IMediaDetailsResponse {
  data: IMediaDetailed;
}

export interface IMediaVideosResponse {
  data: {
    results: IVideo[];
  }
}
