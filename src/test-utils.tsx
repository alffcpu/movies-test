import React from 'react'
import {render, RenderOptions} from '@testing-library/react'
import StateProvider from './state';
import { createMemoryHistory } from "history";
import { Router } from "react-router";

import { unmountComponentAtNode } from "react-dom";

export let container: any;
export const commonBeforeEach = beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

export const commonAfterEach = afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

export const history = createMemoryHistory();

const AllTheProviders: React.FC = ({ children }) => {
  return (
    <Router history={history}>
      <StateProvider>
        {children}
      </StateProvider>
    </Router>
  )
};

const customRender = (ui: React.ReactElement, options: Omit<RenderOptions, 'queries'>) =>
  render(ui, { wrapper: AllTheProviders, ...options });
export * from '@testing-library/react';

export { customRender as render }
