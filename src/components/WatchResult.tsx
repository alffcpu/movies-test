import React, {useCallback, useMemo} from 'react';
import MoviesList from 'components/MoviesList/MoviesList';
import useStore from 'hooks/useStore';
import {watchPerPage} from 'components/ShowList/ShowList';

interface ISearchResultProps {
  onPageChange(page: number): void;
  title: string;
}

const WatchResult: React.FC<ISearchResultProps> = ({onPageChange, title}) => {
  const {detailedListPage, detailedList, detailedListTotal, watchList, favoritesList} = useStore();
  const page = detailedListPage || 1;
  const handleWatchChange = useCallback(() => onPageChange(page), [onPageChange, page]);
  return useMemo(() =>(<MoviesList
      onPageChange={(page) => {
        onPageChange(page);
      }}
      onWatchChange={handleWatchChange}
      items={detailedList}
      total={detailedListTotal}
      title={title}
      current={page}
      pageSize={watchPerPage}
    />),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [watchList, favoritesList, detailedList, detailedListTotal, page]
  );
};

export default WatchResult;

