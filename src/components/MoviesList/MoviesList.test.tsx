import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach, fireEvent} from 'test-utils';
import MoviesList from './MoviesList';
import searchResults from 'stubs/search_stargate';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Movies list rendering and interactions', () => {
  let pageChanged = 0;
  it('without pagination', () => {
    act(() => {
      render(<MoviesList
        current={1}
        items={searchResults}
        onPageChange={() => pageChanged++}
        title={'Search title'}
        total={searchResults.length}
      />, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
