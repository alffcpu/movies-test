import React, {useCallback, useMemo} from 'react';
import {IMedia} from 'types/interfaces';
import {Typography, List, Pagination, message} from 'antd';
import MovieCard from 'components/MovieCard/MovieCard';
import {addToStorageList, removeFromStorageList, StorageLogExt} from 'helpers/searchHistory';
import ErrorMovieCard from 'components/ErrorMovieCard/ErrorMovieCard'
import useStore from '../../hooks/useStore';

interface IMoviesListProps {
  items: null | IMedia[];
  total: null | number;
  current: number;
  title: string;
  pageSize?: number;
  onPageChange(page: number): void;
  onWatchChange?(id: string): void;
}

export const defaultPageSize: number = parseInt(`${process.env.REACT_APP_TMDB_PAGE_SIZE}`, 10) || 20;

const emptyCallback = () => undefined;
const MoviesList: React.FC<IMoviesListProps> = ({onPageChange, items, total, current, title, onWatchChange = emptyCallback, pageSize = defaultPageSize}) => {

  const {favoritesList, setFavorites, watchList, setWatch} = useStore();

  const addFav: StorageLogExt = useCallback((id: string, title: string = '') => {
      message.success(`«${title}» added to favorites`, 2);
      addToStorageList(favoritesList, setFavorites, id, true);
    }, [favoritesList, setFavorites]
  );
  const removeFav: StorageLogExt = useCallback((id: string, title: string = '') => {
      message.warning(`«${title}» removed from favorites`, 2);
      removeFromStorageList(favoritesList, setFavorites, id);
    }, [favoritesList, setFavorites]
  );
  const addWatch: StorageLogExt = useCallback((id: string, title: string = '') => {
      message.success(`«${title}» added to watch list`, 2);
      addToStorageList(watchList, setWatch, id, true);
      onWatchChange(id);
    }, [watchList, setWatch, onWatchChange]
  );
  const removeWatch: StorageLogExt = useCallback((id: string, title: string = '') => {
      message.warning(`«${title}» removed from watch list`, 2);
      removeFromStorageList(watchList, setWatch, id);
      onWatchChange(id);
    }, [watchList, setWatch, onWatchChange]
  );

  const pager = useMemo(() =>
    total && ((total > pageSize) || current > Math.ceil(total / pageSize)) ?
        <Pagination
          pageSize={pageSize}
          showSizeChanger={false}
          className="movies-list-pagination"
          current={current}
          onChange={onPageChange}
          total={total} /> :
        null,
    [total, current, onPageChange, pageSize]);
  return (<>
    <Typography.Title level={3}>{title}{!!total && <small>{`found: ${total}`}</small>}</Typography.Title>
    {pager}
    <List
      bordered
      dataSource={items || []}
      renderItem={(movie) =>
        movie.error ?
          <ErrorMovieCard
            key={movie.id}
	          movie={movie}
	          removeFav={removeFav}
	          removeWatch={removeWatch}
	          watchList={watchList}
	          favoritesList={favoritesList}
          /> :
          <MovieCard
            key={movie.id}
            movie={movie}
            addFav={addFav}
            removeFav={removeFav}
            addWatch={addWatch}
            removeWatch={removeWatch}
            watchList={watchList}
            favoritesList={favoritesList}
          />
      }
    />
    {pager}
  </>);
};

export default MoviesList;

