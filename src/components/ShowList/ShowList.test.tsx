import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach} from 'test-utils';
import ShowList from './ShowList';
import useFetchDetails from 'hooks/useFetchDetails';
import searchResults from 'stubs/search_stargate';
import {IMedia} from 'types/interfaces';
import {getWatchId} from 'helpers/watchId';

jest.mock('hooks/useFetchDetails');

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('watch/favorites list', () => {

  const testList = searchResults.map((item: IMedia) => getWatchId(item));

  it('rendering', () => {
    (useFetchDetails as any).mockImplementation(() => {
      return {
        pending: false,
        fetchList: () => null,
        detailedList: searchResults,
        detailedListTotal: testList.length,
        detailedListPage: 1,
        setStateValue: () => null,
        showList: testList
      };
    });
    act(() => {
      render(<ShowList title="show list title" listName="watchList" />, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
