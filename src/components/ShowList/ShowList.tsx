import React, {useCallback, useEffect} from 'react';
import {Typography, Skeleton, Spin} from 'antd';
import WatchResult from 'components/WatchResult';
import {LoadingOutlined } from '@ant-design/icons';
import useFetchDetails from 'hooks/useFetchDetails';

export const watchPerPage = 10;

export type ListName = 'watchList' | 'favoritesList';

interface IShowListProps {
  listName?: ListName;
  title: string;
}

const ShowList: React.FC<IShowListProps> = ({listName = 'watchList', title}) => {
  const {pending, detailedListTotal, fetchList, setStateValue, detailedListPage, showList} = useFetchDetails(listName);

  useEffect(() => {
    fetchList(detailedListPage);
  }, [detailedListPage, listName, fetchList, showList]);

  const handlePageChange = useCallback((page) => setStateValue({detailedListPage: page}), [setStateValue]);
  return (<>
    {detailedListTotal === null ?
      <>
        <Typography.Title level={3}>{title}</Typography.Title>
        <Skeleton />
        <Skeleton />
        <Skeleton />
      </>:
      <Spin indicator={<LoadingOutlined spin />} size="large" spinning={pending}>
        <WatchResult title={title} onPageChange={handlePageChange} />
      </Spin>}
  </>);
};

export default ShowList;
