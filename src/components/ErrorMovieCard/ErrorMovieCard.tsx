import React from 'react';
import {IMedia} from 'types/interfaces';
import {Alert, Button} from 'antd';
import {StorageLogExt} from 'helpers/searchHistory';
import {CheckOutlined, StarFilled } from '@ant-design/icons';
import useLists from 'hooks/useLists';

interface IMovieCardProps {
  movie: IMedia;
  favoritesList: string[];
  watchList: string[];
  removeFav: StorageLogExt;
  removeWatch: StorageLogExt;
}

const ErrorMovieCard: React.FC<IMovieCardProps> = ({movie, favoritesList = [], watchList = [], removeFav, removeWatch}) => {
  const {id: mediaId, media_type: type, error} = movie;
  const {watchId, isFav, isWatch} = useLists(movie, favoritesList, watchList);

  const genericName = `(${type.toUpperCase()} ID: ${mediaId})`;
  return (<Alert
    message={<strong>Item unavailable. {genericName}</strong>}
    description={<>
      <p>{`Error: ${error}`}</p>
      <p>
        {isFav &&
          <Button onClick={() => removeFav(watchId, genericName)}
            style={{marginRight: '20px'}}
            type="danger"><StarFilled /> Remove from favorites</Button>}
        {isWatch &&
          <Button onClick={() => removeWatch(watchId, genericName)}
            type="danger"><CheckOutlined /> Remove from watch list</Button>}
      </p>
    </>}
    type="error"
    showIcon
  />);
};

export default ErrorMovieCard;

