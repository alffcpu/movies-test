import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach, fireEvent} from 'test-utils';
import ErrorMovieCard from './ErrorMovieCard';
import mediaList from 'stubs/search_stargate';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Movie card error', () => {
  const movie = {
    ...mediaList[0],
    error: 'Fetch error'
  };
  const removed = {
    fav: 0,
    watch: 0
  };
  const watchId = `${movie.media_type}_${movie.id}`;

  it('rendering and interactions with remove buttons', () => {
    act(() => {
      const {getByText} = render(<ErrorMovieCard
        movie={movie}
        favoritesList={[watchId]}
        removeFav={() => removed.fav++}
        removeWatch={() => removed.watch++}
        watchList={[watchId]}
      />, {container});

      fireEvent.click(getByText('Remove from favorites'));
      fireEvent.click(getByText('Remove from watch list'));
    });

    expect(container).toMatchSnapshot();
    expect(removed).toEqual({
      fav: 1,
      watch: 1
    });
  });
  it('rendering without remove buttons', () => {
    act(() => {
      render(<ErrorMovieCard
        movie={movie}
        favoritesList={['1']}
        removeFav={() => null}
        removeWatch={() => null}
        watchList={['movie_1']}
      />, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
