import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach, fireEvent} from 'test-utils';
import MovieCard from './MovieCard';
import PosterModal from './PosterModal';
import VideoModal from './VideoModal';
import mediaList from 'stubs/search_stargate';
import {IKeyValue, IMedia, IVideo} from 'types/interfaces';
import genres from 'stubs/genres';
import useStore from 'hooks/useStore';
import {movie_4629} from 'stubs/movies';

jest.mock('hooks/useStore');
jest.useFakeTimers();

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Movie card', () => {

  const videosResult: {results: IVideo[]} = movie_4629 as any;

  const movie: IMedia = {
    ...mediaList[1],
    videos: videosResult
  };
  const watchId = `${movie.media_type}_${movie.id}`;
  const clicked: IKeyValue = {
    fav: 0,
    watch: 0
  };

  const addClick = (key: string, count: number = 1) =>  clicked[key] += count;

  (useStore as any).mockReturnValue({
    genres
  });

  const clickTrackers = {
    addFav: () => addClick('fav'),
    removeFav: () => addClick('fav', -1),
    addWatch: () => addClick('watch'),
    removeWatch: () => addClick('watch', -1)
  };

  it('card rendering and interactions with fav. and watch', () => {
    act(() => {
      const {getByTitle} = render(<MovieCard
        {...clickTrackers}
        movie={movie}
        watchList={[]}
        favoritesList={[]}
      />, {container});
      fireEvent.click(getByTitle('Add to favorites'));
      fireEvent.click(getByTitle('Add to watch list'));
    });
    expect(container).toMatchSnapshot();
    expect(clicked).toEqual({
      fav: 1,
      watch: 1
    });
  });

  it('card fav. and watch items removal', () => {
    act(() => {
      // const {getByTitle, getByAltText, getByText} = render(<MovieCard
      const {getByTitle, getByAltText, getByText} = render(<MovieCard
        {...clickTrackers}
        movie={movie}
        watchList={[watchId]}
        favoritesList={[watchId]}
      />, {container});
      fireEvent.click(getByTitle('Remove from favorites'));
      fireEvent.click(getByTitle('Remove from watch list'));

      // fireEvent.click(getByAltText('Stargate SG-1'));
      // fireEvent.click(getByText('Stargate SG-1 Opening season 7 HD'));
    });
    expect(container).toMatchSnapshot();
    expect(clicked).toEqual({
      fav: 0,
      watch: 0
    });
  });

  const title = (movie.original_title || movie.original_name) || 'title';
  const image = (movie.poster_path || movie.backdrop_path) || 'image';

  const modals = {
    poster: true,
    video: true,
  };

  it('poster modal', () => {
    act(() => {
      render(<PosterModal
        title={title}
        visible={modals.poster}
        image={image}
        altText={title}
        closeModal={() => modals.poster = false}
        getContainer={container}
      />, container);
      jest.advanceTimersByTime(500);
    });

    expect(container).toMatchSnapshot();
    fireEvent.click(container.querySelector('.ant-btn.ant-btn-primary'));
    expect(modals.poster).toBeFalsy();
  });

  it('video modal', () => {
    act(() => {
      render(<VideoModal
        closeModal={() => modals.video = false}
        data={modals.video ? videosResult.results[0] : null}
        getContainer={container}
      />, container);
      jest.advanceTimersByTime(500);
    });

    expect(container).toMatchSnapshot();
    fireEvent.click(container.querySelector('.ant-btn.ant-btn-primary'));
    expect(modals.video).toBeFalsy();
  });

});
