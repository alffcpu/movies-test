import React, {MutableRefObject, useEffect, useRef} from 'react';
import {message, Modal} from 'antd';
import {SyncOutlined} from '@ant-design/icons';
import {usePreviousValue} from 'beautiful-react-hooks';
import sleep from 'helpers/sleep';
import {IVideo} from 'types/interfaces';
import {ModalProps} from 'antd/es/modal';

interface IPosterModalProps extends ModalProps {
  data: IVideo | null;
  closeModal(): void;
}

const startLoading = async (iframeEl: MutableRefObject<HTMLIFrameElement | null>, iframeSpin: MutableRefObject<HTMLSpanElement | null>, closeModal: () => void, data: IVideo | null) => {
  await sleep(300);
  if (iframeEl.current) {
    const iframe = iframeEl.current;
    iframe.src = `https://www.youtube.com/embed/${data?.key}`;
    iframe.addEventListener('load', () => {
      if (iframeSpin.current) {
        iframeSpin.current.style.visibility = 'hidden'
      }
      iframe.setAttribute('style', '');
    });
    iframe.addEventListener('error', () => {
      closeModal();
      message.error(`Unable to load «${data?.name}»`);
    });
  }
};

const VideoModal: React.FC<IPosterModalProps> = ({data, closeModal, getContainer}) => {
  const iframeEl = useRef<HTMLIFrameElement>(null);
  const iframeSpin = useRef<HTMLSpanElement>(null);
  const visible = !!data;
  const title = data?.name;
  const lastVisible = !!usePreviousValue(visible);

  useEffect(() => {
    if (visible && visible !== lastVisible) {
      startLoading(iframeEl, iframeSpin, closeModal, data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lastVisible, visible]);

  return (<Modal
    centered
    destroyOnClose
    title={<strong>{title}</strong>}
    visible={visible}
    cancelButtonProps={{className: 'btn-hidden'}}
    onCancel={closeModal}
    onOk={closeModal}
    okText="Close"
    getContainer={getContainer}>
    <div className="poster-large-preview">
      <span ref={iframeSpin}><SyncOutlined spin/> Loading video</span>
      <iframe width="100%"
        ref={iframeEl}
        style={{height: '1px', visibility: 'hidden'}}
        title={title}
        height="315"
        src=""
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen/>
    </div>
  </Modal>);
};

export default VideoModal;
