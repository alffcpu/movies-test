import React, {ReactNode, useState, useMemo, useCallback} from 'react';
import {IMedia, IVideo} from 'types/interfaces';
import {Card, Empty, Typography, Rate, Button, Spin, List, Avatar} from 'antd';
import useStore from 'hooks/useStore';
import displayDate from 'helpers/displayDate';
import moment from 'moment';
import {StarOutlined, SyncOutlined, CheckOutlined, StarFilled, YoutubeOutlined} from '@ant-design/icons';
import KnownForList from 'components/KnownForList/KnownForList';
import { LoadingOutlined } from '@ant-design/icons';
import {StorageLogExt} from 'helpers/searchHistory';
import getPosterPath from 'helpers/getPosterPath';
import PosterModal from 'components/MovieCard/PosterModal';
import VideoModal from 'components/MovieCard/VideoModal';
import useLists from 'hooks/useLists';

interface IMovieCardProps {
  movie: IMedia;
  favoritesList: string[];
  watchList: string[];
  addFav: StorageLogExt;
  removeFav: StorageLogExt;
  addWatch: StorageLogExt;
  removeWatch: StorageLogExt;
}

const {Text} = Typography;
const MovieCard: React.FC<IMovieCardProps> = ({movie, favoritesList = [], watchList = [], addFav, removeFav, addWatch, removeWatch}) => {
  const [pending] = useState<boolean>(false);
  const [modal, setModal] = useState<boolean>(false);
  const [videoModal, setVideoModal] = useState<IVideo | null>(null);
  const {genres} = useStore();

  const {watchId, isFav, isWatch} = useLists(movie, favoritesList, watchList);

  const {
    original_title: origTitle = '',
    original_name: origName = '',
    name,
    overview,
    poster_path: posterPath = '',
    backdrop_path: backdropPath = '',
    release_date: releaseDate = '',
    first_air_date: firstAirDate = '',
    genre_ids: genreIds = [],
    media_type: type,
    vote_average: rating,
    known_for: knownFor = [],
    videos,
  } = movie;

  const date = releaseDate || firstAirDate;
  const title = origTitle || origName;
  const image = posterPath || backdropPath;
  const genresList: ReactNode[] = genreIds.reduce((result: ReactNode[] ,gid: number) =>
      genres[gid] ? [...result, <span key={gid}>{genres[gid].name}</span>] : result,
  []);
  const isMedia = type !== 'person';
  const cardTitle = isMedia ?
    <Text strong>{title}<Rate disabled defaultValue={rating / 2} /></Text> :
    <Text strong>{name}</Text>;

  const altText = title ? title.replace('"', "'") : '';
  const video: IVideo[] = useMemo(() => {
    return videos?.results ?
      videos.results.filter((item) => item.site === 'YouTube') :
      []
  }, [videos]);

  const closeModal = useCallback(() => setModal(false), []);
  const closeVideoModal = useCallback(() => setVideoModal(null), []);

  return (<Spin indicator={<LoadingOutlined spin />} size="large" spinning={pending}>
    <Card className="movie-card" title={cardTitle} bordered>
      {isMedia ?
        <div className="movie-card-content">
          <div className="movie-card-poster">
            {image ?
              <img key={image} onClick={() => setModal(true)} src={getPosterPath(image)} alt={altText}/> :
              <Empty description={false}/>
            }
          </div>
          <div className="movie-card-overview">
            <Text className="movie-card-overview-text">{overview}</Text>
            {!!genresList.length &&
              <Text className="movie-card-overview-genres">Genre(s): {genresList}</Text>
            }
            {moment(date).isValid() &&
		          <Text className="movie-card-overview-block">Release date: <em>{displayDate(date)}</em></Text>
            }
            {typeof videos === 'undefined' &&
              <div className="movie-card-overview-preload"><SyncOutlined spin /> <span>Searching for videos</span></div>
            }
            {!!video.length &&
              <>
	              <Text className="movie-card-overview-videos">Trailers and teasers:</Text>
                <List
                  className="movie-card-videos"
                  itemLayout="horizontal"
                  dataSource={video}
                  renderItem={({id, key, name, ...rest}: IVideo) => (
                    <List.Item key={`${id}-${key}`} onClick={() => setVideoModal({...rest, id, name, key})}>
                      <List.Item.Meta
                        avatar={<Avatar size="small" icon={<YoutubeOutlined />} />}
                        title={name}
                      />
                    </List.Item>)}
                  />
              </>}
          </div>
          <div className="movie-card-actions">
            <div>
              <Button onClick={() => isFav ? removeFav(watchId, title) : addFav(watchId, title)}
                type={isFav ? 'dashed' : 'default'}
                shape="circle"
                icon={isFav ? <StarFilled /> : <StarOutlined />}
                size="large"
                title={isFav ? 'Remove from favorites' : 'Add to favorites'}
              />
            </div>
            <div>
              <Button onClick={() => isWatch ? removeWatch(watchId, title): addWatch(watchId, title)}
                type={isWatch ? 'primary' : 'default'}
                shape="circle"
                icon={<CheckOutlined />}
                size="large"
                title={isWatch ? 'Remove from watch list' : 'Add to watch list'}
              />
            </div>
          </div>
        </div> :
	      <div className="movie-card-content">
          {knownFor.length ?
            <KnownForList items={knownFor} /> :
            <Empty description="No information about this person"/>
          }
        </div>
      }
    </Card>
    <PosterModal
	    title={title}
      visible={modal}
      closeModal={closeModal}
      altText={altText}
      image={image}
    />
    <VideoModal
      closeModal={closeVideoModal}
      data={videoModal}
    />
  </Spin>);
};

export default MovieCard;

