import React from 'react';
import getPosterPath from 'helpers/getPosterPath';
import {Modal} from 'antd';
import {ModalProps} from 'antd/es/modal';

interface IPosterModalProps extends ModalProps {
  title: string;
  visible: boolean;
  image: string;
  altText: string;
  closeModal(): void;
}

const PosterModal: React.FC<IPosterModalProps> = ({title, visible, closeModal, image, altText, getContainer}) =>
  (<Modal
    getContainer={getContainer}
    centered
    title={title}
    visible={visible}
    cancelButtonProps={{className: 'btn-hidden'}}
    onCancel={closeModal}
    onOk={closeModal}
    okText="Close"
  >
    <div className="poster-large-preview">
      <img src={getPosterPath(image, 'lg')} alt={altText} />
    </div>
  </Modal>);

export default PosterModal;
