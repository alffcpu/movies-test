import {useEffect} from 'react';
import {useLocation} from 'react-router-dom';
import scrollTop from 'helpers/scrollTop';

//window.scrollTo(0, 0)
const ScrollToTop: React.FC = () => {
  const location = useLocation();
  useEffect(
    () => scrollTop(),
    [location]
  );
  return null;
};

export default ScrollToTop;
