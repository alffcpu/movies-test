import React, {ReactNode, useCallback} from 'react';
import {Menu} from 'antd';
import {MenuOutlined} from '@ant-design/icons';
import {useHistory} from 'react-router-dom';

export interface IHorizMenu {
  [key: string]: [ReactNode, string]
}

interface IProps {
  items: IHorizMenu;
}

const {Item} = Menu;
const HorizontalMenu: React.FC<IProps> = ({items}) => {
  const history = useHistory();
  const handleClick = useCallback(({key}) => {
    history.push(`/${key}`);
  }, [history]);
  return (<Menu mode="horizontal"
                selectable={false}
                onClick={handleClick}
                overflowedIndicator={<MenuOutlined className="menu-icon" />}>
    {Object.entries(items).map(([key, item]) =>
      (<Item key={key}>{item[0]}{item[1]}</Item>)
    )}
  </Menu>);
};

export default HorizontalMenu;

