import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach, fireEvent, history} from 'test-utils';
import HorizontalMenu, {IHorizMenu} from './HorizontalMenu';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Horizontal menu rendering and interactions', () => {

  const menu: IHorizMenu = {
    one: [<span />, 'Entry one'],
    two: [<span />, 'Entry two'],
    three: [<span />, 'Entry three']
  };

  const clickedItems: {[key: string]: number} = {};

  it('without subtitle', () => {
    act(() => {
      jest.spyOn(history, 'push').mockImplementation((key: any) => {
        clickedItems[key] ? clickedItems[key]++ : clickedItems[key] = 1;
      });
      const {getByText} = render(<HorizontalMenu items={menu} />, {container});
      fireEvent.click(getByText('Entry two'));
      fireEvent.click(getByText('Entry two'));
      fireEvent.click(getByText('Entry three'));
    });
    expect(container).toMatchSnapshot();
    expect(clickedItems).toEqual({
      '/two': 2,
      '/three': 1
    });

  });
});
