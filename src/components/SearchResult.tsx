import React, {useMemo} from 'react';
import MoviesList from 'components/MoviesList/MoviesList';
import useStore from 'hooks/useStore';

interface ISearchResultProps {
  onPageChange(page: number): void;
}

const SearchResult: React.FC<ISearchResultProps> = ({onPageChange}) => {
  const {searchResults, searchResultsTotal, searchQuery, searchResultsPage} = useStore();
  return useMemo(() =>
      <MoviesList
        onPageChange={onPageChange}
        items={searchResults}
        total={searchResultsTotal}
        title={`Search results for «${searchQuery}»`}
        current={searchResultsPage}
      />,
    [searchResults, searchQuery, searchResultsTotal, searchResultsPage, onPageChange]);
};

export default SearchResult;

