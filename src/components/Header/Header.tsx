import React from 'react';
import {Layout, Skeleton} from 'antd';
import {SearchOutlined, CheckOutlined, HistoryOutlined, StarOutlined} from '@ant-design/icons';
import HorizontalMenu, {IHorizMenu} from '../HorizontalMenu/HorizontalMenu';
import tmdbLogo from 'assets/images/tmdb_logo2.svg';
import {Link} from 'react-router-dom';

interface IHeaderProps {
  ready?: boolean;
}

const defaultMenu: IHorizMenu = {
  search: [<SearchOutlined />, 'Search'],
  favorites: [<StarOutlined />, 'Favorites'],
  watch: [<CheckOutlined />, 'Watch List'],
  history: [<HistoryOutlined />, 'History']
};

const Header: React.FC<IHeaderProps> = ({ready}) => {
  return (<Layout.Header>
    {ready ?
      <div className="header-content">
        <h2><Link to="/search">{process.env.REACT_APP_TITLE}</Link> <span className="tmdb-attribution">powered by <img
          src={tmdbLogo} alt=''/></span></h2>
        <HorizontalMenu items={defaultMenu}/>
      </div> :
      <Skeleton.Input/>
    }
  </Layout.Header>);
};

export default Header;

