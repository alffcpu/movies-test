import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach} from 'test-utils';
import Header from './Header';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Header rendering', () => {
  it('pending state', () => {
    act(() => {
      render(<Header ready={false}/>, {container});
    });
    expect(container).toMatchSnapshot();
  });
  it('ready state', () => {
    act(() => {
      render(<Header ready={true}/>, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
