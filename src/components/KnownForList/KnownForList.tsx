import React from 'react';
import {IMedia} from 'types/interfaces';
import {Avatar, List} from 'antd';
import {Link} from 'react-router-dom';
import getPosterPath from 'helpers/getPosterPath';

interface IMoviesListProps {
  items: null | IMedia[];
}

export const searchLink = (query: string) => `/search/${encodeURIComponent(query)}`;

const KnownForList: React.FC<IMoviesListProps> = ({items}) => {
  return (<List
    className="knownfor-list"
    bordered={false}
    dataSource={items || []}
    renderItem={({original_title = '', original_name = '', poster_path = '', backdrop_path = ''}) => {
      const title = original_title || original_name;
      const image = poster_path || backdrop_path;
      return (<List.Item>
        <Link to={searchLink(title)} className="knownfor-item">
          {!!image &&
		  <Avatar shape="square" src={getPosterPath(image)} size={32} />}
          <span>{title}</span>
        </Link>
      </List.Item>);
    }}
  />);
};

export default KnownForList;
