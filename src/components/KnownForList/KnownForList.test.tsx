import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach, fireEvent, history} from 'test-utils';
import KnownForList from './KnownForList';
import mediaList from 'stubs/search_stargate';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('KnownForList rendering', () => {
  it('without subtitle', () => {
    act(() => {
      render(<KnownForList items={mediaList} />, {container});
    });
    expect(container).toMatchSnapshot();
  });
});

