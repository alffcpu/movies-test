import React from 'react';
import { Result } from 'antd';

interface IErrorProps {
  title: string;
  subTitle?: string;
}

const Error: React.FC<IErrorProps> = ({title, subTitle}) => {
  return (<Result
    status="error"
    title={title}
    subTitle={subTitle}
  />);
};

export default Error;
