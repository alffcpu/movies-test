import React from 'react';
import {render, act, commonAfterEach, container, commonBeforeEach} from 'test-utils';
import Error from './Error';

beforeEach = commonBeforeEach;
afterEach = commonAfterEach;

describe('Error rendering', () => {
  it('with subtitle', () => {
    act(() => {
      render(<Error title="Error title" subTitle="Error subtitle" />, {container});
    });
    expect(container).toMatchSnapshot();
  });
  it('without subtitle', () => {
    act(() => {
      render(<Error title="Error title" />, {container});
    });
    expect(container).toMatchSnapshot();
  });
});
