import React from 'react';
import ShowList from 'components/ShowList/ShowList';
import useDetailedPageReset from 'hooks/useDetailedPageReset';

const WatchPage: React.FC = () => {
  const ready = useDetailedPageReset();
  return ready ? <ShowList title="My watch list" listName="watchList"/> : null;
};

export default WatchPage;
