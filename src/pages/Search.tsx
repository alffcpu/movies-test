import React, {useCallback, useState, useEffect, Dispatch, SetStateAction} from 'react';
import {Col, Row, Input, Form, Spin, Empty} from 'antd';
import {useDebouncedFn} from 'beautiful-react-hooks';
import useStore from '../hooks/useStore';
import {IState, SetStateValueCallback} from 'state/store';
import {getMovies, CancelGetMovies} from '../backend/search';
import displayResponseError from 'helpers/displayResponseError';
import trim from 'lodash/trim';
import {IMedia, IMediaVideosResponse, ISearchMoviesData, IVideo} from 'types/interfaces';
import {AxiosResponse} from 'axios';
import SearchResult from 'components/SearchResult';
import {useParams, useHistory} from 'react-router-dom';
import isNull from 'lodash/isNull';
import isFunction from 'lodash/isFunction';
import {addToStorageList, StorageLog} from 'helpers/searchHistory';
import {LoadingOutlined} from '@ant-design/icons';
import scrollTop from 'helpers/scrollTop';
import {getMediaVideos} from 'backend/search';

type VideoValue = undefined | {results: IVideo[]};

const getVidos = async (results: IMedia[], setStateValue: SetStateValueCallback) => {
  const inittVal: IMedia[] = [];
  const media: IMedia[] = results.reduce((result, item) => {
    return item.media_type !== 'person' ?
      [...result, item] :
      result;
  }, inittVal);
  const fetchedData: {[key: number]: VideoValue} = {};
  const fetcher = async (mediaType: string, id: number) => {
    try {
      const {data}: IMediaVideosResponse = await getMediaVideos(mediaType, id);
      fetchedData[id] = data;
    } catch (error) {
      fetchedData[id] = {results: []};
    }
  };
  if (media.length) {
    const queue = [];
    for (const item of media) {
      queue.push(fetcher(item.media_type, item.id));
    }
    await Promise.all(queue);
    setStateValue((state: IState) => {
      const {searchResults} = state;
      const newSearchResult = (searchResults || []).map((item) =>
        fetchedData[item.id] ? {...item, videos: fetchedData[item.id]} : item
      );
      return {...state, searchResults: newSearchResult};
    });
  }
};

const searchMovies = async (query: string, oldQuery: string, page: number, setStateValue: SetStateValueCallback, setPending: Dispatch<SetStateAction<boolean>>, historyLog?: StorageLog) => {
  const preparedQuery = trim(`${query}`);
  const setResult = (q: string, r: IMedia[], t: number, p: number) =>
    setStateValue({searchQuery: q, searchResults: r, searchResultsTotal: t, searchResultsPage: p});
  if ((preparedQuery.length > 3) && oldQuery !== preparedQuery) {
    setPending(true);
    try {
      if (!isNull(CancelGetMovies)) CancelGetMovies();
      const {data}: AxiosResponse<ISearchMoviesData> = await getMovies({query, page});
      const {results, total_results: total} = data;
      // console.log(results.map((r) => r.id));
      setResult(preparedQuery, results, total, page);
      if (isFunction(historyLog)) historyLog(preparedQuery);
      getVidos(results, setStateValue);
    } catch (error) {
      displayResponseError(error);
    } finally {
      setPending(false);
    }
  } else if (!preparedQuery && oldQuery) {
    setResult('', [], 0, 1);
  }
};

const useSearchMovies = () => {
  const [pending, setPending] = useState(false);
  const {searchQuery, setStateValue, searchResultsPage, searchHistory, setSearchHistory} = useStore();

  const historyLog: StorageLog = useCallback((query: string) =>
      addToStorageList(searchHistory, setSearchHistory, query),
    [searchHistory, setSearchHistory]
  );

  const search = useCallback((query: string, oldQuery: string = '', page = searchResultsPage) =>
      searchMovies(query, oldQuery, page, setStateValue, setPending, historyLog),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );
  return {
    search,
    searchQuery,
    pending
  }
};

const {useForm, Item} = Form;
const Search: React.FC = () => {
  const [form] = useForm();
  const {title} = useParams();
  const history = useHistory();
  const {search, searchQuery, pending} = useSearchMovies();

  useEffect(() => {
    if (title) {
      const decodedTitle = decodeURIComponent(title);
      form.setFieldsValue({query: decodedTitle});
      search(decodedTitle, '', 1);
      setTimeout(() => history.replace('/search'), 500);
    } else {
      form.setFieldsValue({query: searchQuery});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [title]);

  const handlePageChange = useCallback((page: number) => {
    search(searchQuery, '', page);
    scrollTop();
  }, [search, searchQuery]);

  const handleSearch: unknown = useDebouncedFn(() => {
    const value = form.getFieldValue('query');
    search(value, searchQuery, 1);
  }, 500);
  const handleChangeSubmit = useCallback(() => (handleSearch as Function)(), [handleSearch]);

  return (<>
      <Row>
        <Col span={24}>
          <Form form={form}>
            <Item name="query">
              <Input.Search
                allowClear
                placeholder="Start typing here..."
                size="large"
                onChange={handleChangeSubmit}
                loading={pending}
                onSearch={handleChangeSubmit}
              />
            </Item>
          </Form>
        </Col>
      </Row>
    <Spin indicator={<LoadingOutlined spin />} size="large" spinning={pending}>
      {!!searchQuery ?
        <SearchResult onPageChange={handlePageChange} /> :
        <Empty image={null} description={false} />
      }
    </Spin>
  </>);
};

export default Search;
