import React from 'react';
import { Result } from 'antd';
import { Link } from 'react-router-dom';

const Error: React.FC = () => {
  return (<Result
    status="error"
    title="Page not found!"
    subTitle="Sorry, but the requested page was not found."
    extra={[<Link to="/search">Search</Link>]}
  />);
};

export default Error;
