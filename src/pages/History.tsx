import React from 'react';
import {List, Typography} from 'antd';
import {Link} from 'react-router-dom';
import {searchLink} from 'components/KnownForList/KnownForList';
import useStore from 'hooks/useStore';

const History: React.FC = () => {
  const {searchHistory} = useStore();
  return (<>
    <Typography.Title level={3}>Search history</Typography.Title>
    <List
      className="search-history-list"
      bordered={false}
      dataSource={searchHistory}
      renderItem={(value: string, idx) => <List.Item key={`${idx}-${value}`}>
        <Link to={searchLink(value)}>{idx+1}. {value}</Link>
        </List.Item>}
    />
  </>);
};

export default History;
