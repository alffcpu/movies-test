import React from 'react';
import ShowList from 'components/ShowList/ShowList';
import useDetailedPageReset from 'hooks/useDetailedPageReset';

const FavoritesPage: React.FC = () => {
  const ready = useDetailedPageReset();
  return ready ? <ShowList title="My favorites" listName="favoritesList"/> : null;
};

export default FavoritesPage;
