import React, {createContext, useState} from 'react';
import {initialState, IState} from 'state/store';
import isObject from 'lodash/isObject';
import isUndefined from 'lodash/isUndefined';
import isFunction from 'lodash/isFunction';
import {useLocalStorage} from 'beautiful-react-hooks';

export const searchHistoryKey = 'search-history';
export const favStorageKey = 'favorites-movies';
export const watchStorageKey = 'watch-list-items';

export const AppStateContext = createContext({...initialState});
const StateProvider: React.FC = ({children}) => {
  const [state, setState] = useState({...initialState});
  const [favoritesList, setFavorites] = useLocalStorage(favStorageKey, []);
  const [watchList, setWatch] = useLocalStorage(watchStorageKey, []);
  const [searchHistory, setSearchHistory] = useLocalStorage(searchHistoryKey, []);

  const value: IState = {
    ...state,
    setFavorites,
    setWatch,
    setSearchHistory,
    get watchList () {
      return watchList;
    },
    get favoritesList() {
      return favoritesList;
    },
    get searchHistory() {
      return searchHistory;
    },
    setStateValue: (key, value) => {
      setState((state) => {
        if (isFunction(key)) {
          return key(state);
        }
        const changes = !isUndefined(value) ?
          {[`${key}`]: value} :
            isObject(key) ?
              {...key} :
              {};
        return {...state, ...changes};
      });
    },
  };
  return (<AppStateContext.Provider value={value}>{children}</AppStateContext.Provider>);
};

export default StateProvider;
