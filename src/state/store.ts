import {IMedia, IGenres} from 'types/interfaces';
import {ListName} from 'components/ShowList/ShowList';

export interface IState {
  genres: IGenres;
  searchQuery: string;
  searchResults: null | IMedia[];
  searchResultsTotal: null | number;
  searchResultsPage: number;
  setStateValue: SetStateValueCallback;
  detailedList: null | IMedia[];
  detailedListName: null | ListName;
  detailedListPage: number;
  detailedListTotal: null | number;
  favoritesList: string[];
  watchList: string[];
  searchHistory: string[];
  setFavorites: LSCallback;
  setWatch: LSCallback;
  setSearchHistory: LSCallback;
}

const emptyCallback = () => undefined;

export const initialState: IState = {
  genres: {},
  searchResults: null,
  searchResultsPage: 0,
  searchResultsTotal: null,
  searchQuery: '',
  detailedList: null,
  detailedListName: null,
  detailedListPage: 1,
  detailedListTotal: null,
  favoritesList: [],
  watchList: [],
  searchHistory: [],
  setStateValue: emptyCallback,
  setFavorites: emptyCallback,
  setWatch: emptyCallback,
  setSearchHistory: emptyCallback,
};

type LSCallback = (newState: string[]) => void;
type SSVCallback = (state: IState) => IState;
export type SetStateValueCallback = (key: (keyof IState & string) | Partial<IState> | SSVCallback, value?: any) => void;
