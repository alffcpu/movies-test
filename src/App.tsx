import React, {useState, useEffect, Dispatch, SetStateAction} from 'react';
import {Layout, Skeleton, BackTop} from 'antd';
import Header from 'components/Header/Header';
import SearchPage from 'pages/Search';
import WatchPage from 'pages/WatchPage';
import FavoritesPage from 'pages/FavoritesPage';
import HistoryPage from 'pages/History';
import ErrorPage from 'pages/Error';
import {Switch, Route} from 'react-router-dom';
import {fetchGenres} from 'backend/genre';
import useStoreValue from 'hooks/useStoreValue';
import {SetStateValueCallback} from 'state/store';
import Error from 'components/Error/Error';
import {useLocation} from 'react-router-dom';

export type  DispatchAny<T> = Dispatch<SetStateAction<T>>;

const initData = async (setStoreValue: SetStateValueCallback, setReady: DispatchAny<boolean>, setError: DispatchAny<boolean>) => {
  try {
    const genres = await fetchGenres();
    setStoreValue('genres', genres);
    setReady(true);
  } catch (error) {
    setError(true);
  }
};

const {Content} = Layout;

const App: React.FC = () => {
  const [ready, setReady] = useState(false);
  const [error, setError] = useState(false);
  const [,setStoreValue] = useStoreValue();
  const location = useLocation();

  useEffect(() => {
    initData(setStoreValue, setReady, setError);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (<Layout>
    <Header ready={ready} />
    <Content>
      {ready ?
        <Switch location={location}>
          <Route exact path={['/search', '/', '/search/:title']}><SearchPage /></Route>
          <Route exact path="/favorites"><FavoritesPage /></Route>
          <Route exact path="/watch"><WatchPage /></Route>
          <Route exact path="/history"><HistoryPage /></Route>
          <Route><ErrorPage /></Route>
        </Switch> :
          (error ?
            <Error title="Initialization error" />:
            <Skeleton active />)
      }
      <BackTop visibilityHeight={400} />
    </Content>
  </Layout>);
};

export default App;
