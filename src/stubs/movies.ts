export const movie_4629 = {
  "id": 4629,
  "results": [
    {
      "id": "552d95e2925141387300647b",
      "iso_639_1": "en",
      "iso_3166_1": "US",
      "key": "5qfQHEj_f4Y",
      "name": "Stargate SG-1 Opening season 7 HD",
      "site": "YouTube",
      "size": 720,
      "type": "Opening Credits"
    },
    {
      "id": "5b1728530e0a262ddf01e474",
      "iso_639_1": "en",
      "iso_3166_1": "US",
      "key": "Lf7Re7DGzJg",
      "name": "Stargate SG-1(season 3).",
      "site": "YouTube",
      "size": 720,
      "type": "Trailer"
    },
    {
      "id": "5a712e32c3a368577300023d",
      "iso_639_1": "en",
      "iso_3166_1": "US",
      "key": "wSRS7c_UqaM",
      "name": "STARGATE SG1 season 1 Trailer #1 - Richard Dean Anderson",
      "site": "YouTube",
      "size": 720,
      "type": "Trailer"
    }
  ]
};

export const movie_2164 = {
  "id": 2164,
  "results": [
    {
      "id": "58bd0f4cc3a36851cd02e0c9",
      "iso_639_1": "en",
      "iso_3166_1": "US",
      "key": "qMLyCQq6Gvc",
      "name": "Stargate Official Trailer #1 - Kurt Russell Movie (1994) HD",
      "site": "YouTube",
      "size": 480,
      "type": "Trailer"
    },
    {
      "id": "54d22bc1c3a368307600270b",
      "iso_639_1": "en",
      "iso_3166_1": "US",
      "key": "GZwcZ8TBlaU",
      "name": "Stargate - The Ultimate Edition (2003) Promo (VHS Capture)",
      "site": "YouTube",
      "size": 480,
      "type": "Trailer"
    }
  ]
};
