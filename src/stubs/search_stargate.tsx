import {IMedia} from 'types/interfaces';

export const search_stargate: IMedia[] = [
  {
    "poster_path": "/fyTqTETiXncfESLyKfkgEs7qg7d.jpg",
    "popularity": 17.105,
    "vote_count": 1911,
    "media_type": "movie",
    "id": 2164,
    "adult": false,
    "backdrop_path": "/w7OqQxudcdYEuYdIyjEB7LzWrRQ.jpg",
    "original_language": "en",
    "original_title": "Stargate",
    "genre_ids": [
      28,
      12,
      878
    ],
    "title": "Stargate",
    "vote_average": 6.9,
    "overview": "An interstellar teleportation device, found in Egypt, leads to a planet with humans resembling ancient Egyptians who worship the god Ra.",
    "release_date": "1994-10-27"
  },
  {
    "original_name": "Stargate SG-1",
    "genre_ids": [
      10759,
      10765
    ],
    "media_type": "tv",
    "name": "Stargate SG-1",
    "popularity": 37.77,
    "origin_country": [
      "CA",
      "US"
    ],
    "vote_count": 629,
    "first_air_date": "1997-07-27",
    "backdrop_path": "/ul7W8lwLIgxre1LXigFw55upfZ5.jpg",
    "original_language": "en",
    "id": 4629,
    "vote_average": 8.1,
    "overview": "The story of Stargate SG-1 begins about a year after the events of the feature film, when the United States government learns that an ancient alien device called the Stargate can access a network of such devices on a multitude of planets. SG-1 is an elite Air Force special operations team, one of more than two dozen teams from Earth who explore the galaxy and defend against alien threats such as the Goa'uld, Replicators, and the Ori.",
    "poster_path": "/rst5xc4f7v1KiDiQjzDiZqLtBpl.jpg"
  },
  {
    "original_name": "Stargate Atlantis",
    "genre_ids": [
      18,
      10759,
      10765
    ],
    "media_type": "tv",
    "name": "Stargate Atlantis",
    "popularity": 21.463,
    "origin_country": [
      "US",
      "CA"
    ],
    "vote_count": 422,
    "first_air_date": "2004-07-15",
    "backdrop_path": "/9ix0E6HxMh3zTR1UFsKRMZIMnIr.jpg",
    "original_language": "en",
    "id": 2290,
    "vote_average": 7.9,
    "overview": "With the Ancients' city of Atlantis discovered in the Pegasus Galaxy by Stargate Command, Dr. Elizabeth Weir and Major Sheppard lead a scientific expedition to the ancient abandoned city. Once there, the team not only find themselves unable to contact Earth, but their explorations unexpectedly reawaken the Ancients' deadly enemies, The Wraith, who hunger for this new prey. Now with the help of newfound local allies like Teyla Emmagan, the Atlantis Team sets about to uncover their new home's secrets even as their war of survival against the Wraith begins.",
    "poster_path": "/zfFjkOSK18SVrFRvvwrMrt6bEPy.jpg"
  },
  {
    "poster_path": "/lTghD4VOTQ6dF9yQuXUMpfbb8wf.jpg",
    "popularity": 10.233,
    "vote_count": 248,

    "media_type": "movie",
    "id": 12914,
    "adult": false,
    "backdrop_path": "/jT7tdu9ksHkdS4R13ieXNs5efYW.jpg",
    "original_language": "en",
    "original_title": "Stargate: Continuum",
    "genre_ids": [
      12,
      878,
      10770
    ],
    "title": "Stargate: Continuum",
    "vote_average": 7,
    "overview": "Ba'al travels back in time and prevents the Stargate program from being started. SG-1 must somehow restore history.",
    "release_date": "2008-07-29"
  },
  {
    "original_name": "Stargate Universe",
    "id": 5148,
    "media_type": "tv",
    "name": "Stargate Universe",
    "popularity": 14.096,
    "vote_count": 317,
    "vote_average": 7.2,
    "first_air_date": "2009-10-02",
    "poster_path": "/nq9ga7YkixdXx0QgDEJ0U19bSR8.jpg",
    "genre_ids": [
      10759,
      18,
      10765
    ],
    "original_language": "en",
    "backdrop_path": "/78v4SKqgulgJSjunvAvPNO2mBIh.jpg",
    "overview": "The adventures of a present-day, multinational exploration team traveling on the Ancient spaceship Destiny many billions of light years distant from the Milky Way Galaxy. They evacuated there and are now trying to figure out a way to return to Earth, while simultaneously trying to explore and to survive in their unknown area of the universe.",
    "origin_country": [
      "CA",
      "US"
    ]
  },
  {
    "poster_path": "/KmuVZVqC77Lws3F2U1PwLerlhB.jpg",
    "popularity": 10.392,
    "vote_count": 102,

    "media_type": "movie",
    "id": 80527,
    "adult": false,
    "backdrop_path": '',
    "original_language": "en",
    "original_title": "Stargate SG-1: Children of the Gods",
    "genre_ids": [
      12,
      878,
      53
    ],
    "title": "Stargate SG-1: Children of the Gods",
    "vote_average": 7.3,
    "overview": "Remastered, re-cut, re-imagined, remarkable! (keep in mind this is considered by some to be nothing more than censorship, removing the best lines and the only nudity, actually making runtime less, compare the Uncut to Final Cut and make up your own mind). The thrilling pilot episode of television's longest-running sci-fi series. An alien similar to Ra appears out of the Stargate, killing five soldiers and kidnapping another, a year after the original Stargate mission. A new team is assembled, including some old members, and they go in search of the missing soldier in order to find out how Ra could still be alive. Meanwhile, the alien Gou'ald kidnap Sha're and Skaara, implanting them with symbiotes and making them Gou'ald hosts.",
    "release_date": "2009-07-21"
  },
  {
    "poster_path": "/biMD2jBTkbE8odfdubQlTM7xGoO.jpg",
    "popularity": 10.033,
    "vote_count": 241,

    "media_type": "movie",
    "id": 13001,
    "adult": false,
    "backdrop_path": "/c7zQHgs0AAuKRN3yqghIkLN8QiU.jpg",
    "original_language": "en",
    "original_title": "Stargate: The Ark of Truth",
    "genre_ids": [
      12,
      878,
      10770
    ],
    "title": "Stargate: The Ark of Truth",
    "vote_average": 7.1,
    "overview": "SG-1 searches for an ancient weapon which could help them defeat the Ori, and discover it may be in the Ori's own home galaxy. As the Ori prepare to send ships through to the Milky Way to attack Earth, SG-1 travels to the Ori galaxy aboard the Odyssey. The International Oversight committee have their own plans and SG-1 finds themselves in a distant galaxy fighting two powerful enemies.",
    "release_date": "2008-03-11"
  },
  {
    "original_name": "Stargate Origins",
    "genre_ids": [
      10759,
      10765
    ],
    "media_type": "tv",
    "name": "Stargate Origins",
    "popularity": 9.387,
    "origin_country": [
      "US"
    ],
    "vote_count": 48,
    "first_air_date": "2018-02-15",
    "backdrop_path": "/4wN8ciEk2sE3YrET9osXR2AhSaZ.jpg",
    "original_language": "en",
    "id": 72925,
    "vote_average": 4.7,
    "overview": "Follow Catherine Langford, the young woman who witnessed her father uncover the Stargate in Giza in 1928, as she embarks on an unexpected adventure to unlock the mystery of what lies beyond the Stargate in order to save Earth from unimaginable darkness.",
    "poster_path": "/esec4AKT1BVNeQbPMO9rCHHvSvd.jpg"
  },
  {
    "original_name": "Stargate Infinity",
    "id": 2580,
    "media_type": "tv",
    "name": "Stargate Infinity",
    "popularity": 3.362,
    "vote_count": 6,
    "vote_average": 3.8,
    "first_air_date": "2002-09-14",
    "poster_path": "/giHRHloowaDmsm2xTQV1JXV3cNG.jpg",
    "genre_ids": [
      16
    ],
    "original_language": "en",
    "backdrop_path": '',
    "overview": "Stargate Infinity is an American animated science fiction television series and part of Metro-Goldwyn-Mayer's Stargate franchise, but is not considered official Stargate canon. The show was created by Eric Lewald and Michael Maliani, as a spin-off series of Stargate SG-1 created by Brad Wright and Jonathan Glassner after the release of Stargate. The cartoon had a low viewership rating and poor reception; it was canceled after one season. Co-produced by MGM Television and DIC Entertainment and directed by Will Meugniot.\n\nThe story arc of Stargate Infinity follows Gus Bonner being framed for opening the Stargate for alien enemies in a future version of Stargate Command. Bonner escapes with a group of fresh recruits through the Stargate. The team can not return to Earth before they have cleared their names. The show was cancelled before any of its major plots could be resolved. The story unfolds when the members of the team encounters different alien races from other planets.",
    "origin_country": [
      "US"
    ]
  },
  {
    "poster_path": "/3e2gPmE5KDPFpWjDkziOyeho5SD.jpg",
    "popularity": 1.485,
    "vote_count": 6,

    "media_type": "movie",
    "id": 376268,
    "adult": false,
    "backdrop_path": "/jCcvhae8MmkqYtR1EGtMYFdpQud.jpg",
    "original_language": "en",
    "original_title": "Stargate SG-1: True Science",
    "genre_ids": [
      99,
      878,
      10770
    ],
    "title": "Stargate SG-1: True Science",
    "vote_average": 8.2,
    "overview": "In this documentary, Amanda Tapping, known as Samantha Carter from SG-1, shows the scientific background of the successful science fiction series \"Stargate SG-1\" and lets us take a look behind the scenes.",
    "release_date": "2006-01-03"
  },
  {
    "poster_path": "/nrdWXJRV8W86k3diJDfghf1sNKC.jpg",
    "popularity": 1.138,
    "vote_count": 1,

    "media_type": "movie",
    "id": 574161,
    "adult": false,
    "backdrop_path": "/n9UtMkSlev7TwDlaXvqLyVf1yiQ.jpg",
    "original_language": "fr",
    "original_title": "Stargate: Base Gamma",
    "genre_ids": [
      28,
      12,
      878,
      10770
    ],
    "title": "Stargate: Base Gamma",
    "vote_average": 10,
    "overview": "Following an exploration mission on P2S-569, and the discovery of an underground facility intact, the SGC made the decision to use the existing infrastructure to develop a new site: the Gamma Base. By the time the latter is fully operational, General O'Neill has assigned the majority of his manpower available for defense. In parallel, as part of the alliance against the Goa'old's, a fleet of Rebels Jaffas is expected in the system for military exercises and scientific trials. Even more than the ship arming tests, the goal of the allies is the understanding of the \"white dwarf phenomenon\" that stretches around the planet, because it seems to affect most advanced technologies. The SGC garrison is preparing to receive their allies. However, the Jaffas do not seem to be the ones planned, and their goals are more obscure .",
    "release_date": "2017-09-13"
  },
  {
    "original_name": "SGU Stargate Universe Kino",
    "id": 85277,
    "media_type": "tv",
    "name": "SGU Stargate Universe Kino",
    "popularity": 1.4,
    "vote_count": 2,
    "vote_average": 2,
    "first_air_date": "2009-10-29",
    "poster_path": "/AwwIYo0dB9QfX5mTpAyGPYGUKPd.jpg",
    "genre_ids": [
      10765
    ],
    "original_language": "en",
    "backdrop_path": '',
    "overview": "Stranded in a distant part of the universe, the crew of the Ancient starship Destiny record their thoughts and observations.",
    "origin_country": [
      "US"
    ]
  },
  {
    "vote_count": 0,
    "popularity": 0.6,
    "id": 604450,

    "media_type": "movie",
    "vote_average": 0,
    "title": "Stargate Atlantis: Rising",
    "release_date": "2006-09-09",
    "original_language": "en",
    "original_title": "Stargate Atlantis: Rising",
    "genre_ids": [
      878
    ],
    "backdrop_path": '',
    "adult": false,
    "overview": "",
    "poster_path": ''
  },
  {
    "original_name": "Stargate 2012",
    "id": 46852,
    "media_type": "tv",
    "name": "Stargate 2012",
    "popularity": 0.84,
    "vote_count": 0,
    "vote_average": 0,
    "first_air_date": "2005-01-01",
    "poster_path": '',
    "genre_ids": [
      99
    ],
    "original_language": "en",
    "backdrop_path": '',
    "overview": "",
    "origin_country": []
  },
  {
    "poster_path": "/pV8gWYeIQruSZ7tInBI01dYTw5c.jpg",
    "popularity": 0.6,
    "vote_count": 2,

    "media_type": "movie",
    "id": 226412,
    "adult": false,
    "backdrop_path": '',
    "original_language": "en",
    "original_title": "Is There a Stargate?",
    "genre_ids": [
      99,
      878,
      10770
    ],
    "title": "Is There a Stargate?",
    "vote_average": 10,
    "overview": "Documentary about possible ancient alien visitors to Earth.",
    "release_date": "2003-02-18"
  },
  {
    "poster_path": "/lBedGgJlz2uzHrvHT7ZHhXwpSRg.jpg",
    "popularity": 0,
    "vote_count": 0,

    "media_type": "movie",
    "id": 690254,
    "adult": false,
    "backdrop_path": '',
    "original_language": "en",
    "original_title": "Stargate Origins: Catherine",
    "genre_ids": [
      12,
      14,
      36,
      878
    ],
    "title": "Stargate Origins: Catherine",
    "vote_average": 0,
    "overview": "Stargate Origins explores the journey that sets a young Catherine Langford on the way to helping unlock the secrets of an ancient technology that could change the very course of humanity.",
    "release_date": "2018-06-19"
  }
];

export default search_stargate;

