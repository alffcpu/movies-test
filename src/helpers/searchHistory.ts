import chunk from 'lodash/chunk';
import {HandlerSetter} from 'beautiful-react-hooks';
import trim from 'lodash/trim';
import isArray from 'lodash/isArray';

export const searchHistorySize = 50;

export const addItem = (searchHistory: string[], query: string, unlimited: boolean) => {
  const newList = searchHistory.filter((value: string) => value !== query);
  return unlimited ?
    [query, ...newList] :
    [query, ...(newList.length < searchHistorySize ? newList : chunk(newList, searchHistorySize - 1)[0])];
};

export const addToStorageList = (searchHistory: string[], setSearchHistory: HandlerSetter, inQuery: string, unl: boolean = false): string[] => {
  const query = trim(inQuery);
  if (query) {
    const newHistory = !isArray(searchHistory) ?
      [query] :
      addItem(searchHistory, query, unl);
    setSearchHistory(newHistory);
    return newHistory;
  }
  return searchHistory;
};

export const removeFromStorageList = (list: string[], setHandler: HandlerSetter, item: string) => {
  const query = trim(item);
  if (query || !isArray(list)) {
    const newList = list.filter((value) => query !== value);
    setHandler(newList);
    return newList;
  }
  return list;
};

export type StorageLog = (query: string) => void;
export type StorageLogExt = (query: string, title: string | undefined) => void;
