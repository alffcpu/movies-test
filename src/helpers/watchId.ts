import {IMedia} from 'types/interfaces';

export const getWatchId = ({id, media_type}: IMedia) =>
  `${media_type}_${id}`;

export const parseWatchId = (watchId: string) => {
  const [mediaType, id] = watchId.split('_');
  return {id, mediaType};
};

export const makeWatchId = (id: string | number, mediaType: string) =>
  `${mediaType}_${id}`;
