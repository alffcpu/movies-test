import moment, {Moment} from 'moment';

export default (date: string | Moment, format = 'DD MMMM YYYY') =>
  moment(date).format(format);
