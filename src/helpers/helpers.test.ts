import displayDate from './displayDate';
import getPosterPath from './getPosterPath';
import searchResults from 'stubs/search_stargate';

describe('Helpers', () => {

  it('date display', () => {
    const time = displayDate('2020-03-02');
    expect(time).toBe('02 March 2020');
  });

  it('poster path', () => {
    const posterUrl = getPosterPath(searchResults[0].poster_path);
    expect(posterUrl).toBe('http://image.tmdb.org/t/p/w220_and_h330_face/fyTqTETiXncfESLyKfkgEs7qg7d.jpg');
  });

});
