import { message } from 'antd';
import axios, { AxiosError } from 'axios';
import get from 'lodash/get';

export const getErrorMessage = (error: Error | AxiosError) => {
  const errorStatus = get(error, 'response.data.status_message');
  const errorCode= get(error, 'response.data.status_code');
  return errorStatus ?
    `${errorCode ? `[${errorCode}] ` : ''}${errorStatus}` :
    `${error}`;
}

export default (error: AxiosError | Error) => {
  if (!axios.isCancel(error)) {
    message.error(`Server: ${getErrorMessage(error)}`, 5);
  }
}
