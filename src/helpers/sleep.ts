const sleep = (milliseconds = 300) => new Promise((resolve) => setTimeout(resolve, milliseconds));

export default sleep;
