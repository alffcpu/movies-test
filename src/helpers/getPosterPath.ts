import trim from 'lodash/trim';

type PosterSize = 'sm' | 'lg';
const posterSizes = {
  sm: 'w220_and_h330',
  lg: 'w440_and_h660',
};

export const postersUrl = trim(process.env.REACT_APP_TMDB_POSTER_URL, '/');
const getPosterPath = (posterPath: string, posterSize: PosterSize = 'sm') => `${postersUrl}/${posterSizes[posterSize]}_face/${trim(posterPath, '/')}`;

export default getPosterPath;
